﻿#include <iostream>

void PrintNumbers(int N, bool isOdd)
{
    if (isOdd)
    {
        for (int i = 0; i <= N; i += 2)
        {
            std::cout << i << " ";
        }
        std::cout << std::endl;
    }
    else
    {
        for (int k = 1; k <= N; k += 2)
        {
            std::cout << k << " ";
        }
        std::cout << std::endl;
    }
}

int main()
{
    PrintNumbers(100, true);
    PrintNumbers(100, false);
}